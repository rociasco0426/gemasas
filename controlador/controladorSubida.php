<?php
require "../dao/datosFormularioDao.php";
require "../dto/datosFormularioDto.php";
require "../conexion/conexion.php";

if(isset($_POST['enviar'])) {   
    echo "Fichero subido correctamente<br>";
    echo "Fichero subido: '" . $_FILES["fichero"]["name"] . "'<br>";
    

    $archivo    =   fopen($_FILES["fichero"]["tmp_name"], "r");

    while( ($linea =   fgets($archivo)) !== false ){

            $arrayDatos =   explode(",", $linea);
            $email  =   $arrayDatos[0];
            $nombre      =   $arrayDatos[1];   
            $apellido             =   $arrayDatos[2]; 
            $codigo = $arrayDatos[3]; 
            $codigoRevisor = $arrayDatos[4]; 

            $datosFormDao = new datosFormularioDao();
            $datosFormDto = new datosFormularioDto();
            $datosFormDto->setEmail($email);
            $datosFormDto->setNombre($nombre);
            $datosFormDto->setApellidos($apellido);
            $datosFormDto->setCodigo($codigo);
            $datosFormDto->setCodRevisor($codigoRevisor);
            $datosFormDao->insertarDatos($datosFormDto);
            header('Location: ../visualizacionResultados.php');

    }

} 
?>
