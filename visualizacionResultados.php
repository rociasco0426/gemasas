<html>
<head>
<link rel="stylesheet" href="css/bootstrap.css">

</head>
<a href="formularioSubida.php">Volver</a>
<br/>
<label>Usuarios Activos</label>
<?php
require "conexion/conexion.php";
require "dao/datosFormularioDao.php";
$datosDao = new datosFormularioDao();
?>
<table id="datatable" class="table table-danger">
        <thead>
            <th>Email</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Revisor</th>
           </thead>
        <tbody>
            <?php
            $datosFormulario = $datosDao->listarUsuariosActivos();
            foreach ($datosFormulario as $datos) {?>
                <tr>
                <td><?php echo $datos["email"]?></td>
                <td><?php echo $datos["nombre"]?></td>
                <td><?php echo $datos["apellido"]?></td>
                <td><?php echo $datos["nomRevisor"].' '.$datos["apRevisor"]?></td>
                </tr>
             <?php }?>
             
        </tbody>
</table> 
<br/>
<label>Usuarios Inactivos</label>
<table id="datatable" class="table table-danger">
        <thead>
            <th>Email</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Revisor</th>
           </thead>
        <tbody>
            <?php
            $datosFormulario = $datosDao->listarUsuariosInactivos();
            foreach ($datosFormulario as $datos) {?>
                <tr>
                <td><?php echo $datos["email"]?></td>
                <td><?php echo $datos["nombre"]?></td>
                <td><?php echo $datos["apellido"]?></td>
                <td><?php echo $datos["nomRevisor"].' '.$datos["apRevisor"]?></td>
                </tr>
             <?php }?>
             
        </tbody>
</table> 
<br/>
<label>Usuarios En Proceso de Espera</label>
<table id="datatable" class="table table-danger">
        <thead>
            <th>Email</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Revisor</th>
           </thead>
        <tbody>
            <?php
            $datosFormulario = $datosDao->listarUsuariosEspera();
            foreach ($datosFormulario as $datos) {?>
                <tr>
                <td><?php echo $datos["email"]?></td>
                <td><?php echo $datos["nombre"]?></td>
                <td><?php echo $datos["apellido"]?></td>
                <td><?php echo $datos["nomRevisor"].' '.$datos["apRevisor"]?></td>
                </tr>
             <?php }?>
             
        </tbody>
</table> 
</html>
