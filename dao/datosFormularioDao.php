<?php
class datosFormularioDao{
    public function insertarDatos(datosFormularioDto $datosFormularioDto){
        $cnn=Conexion::getConexion();
        try {
        	$query=$cnn->prepare("INSERT INTO datosFormularios (email,nombre,apellido,codigo,codigoRevisor) VALUES (?,?,?,?,?)");
			$query->bindParam(1,$datosFormularioDto->getEmail());
            $query->bindParam(2,$datosFormularioDto->getNombre());
            $query->bindParam(3,$datosFormularioDto->getApellidos());
            $query->bindParam(4,$datosFormularioDto->getCodigo());
            $query->bindParam(5,$datosFormularioDto->getCodRevisor());
            $query->execute();
            $mensaje = "Registro Exitoso";
        } catch (Exception $e) {
        	$mensaje=$e->getMessage();
        }
        $cnn=null;
		return $mensaje;
    }
    
    public function listarUsuariosActivos(){
		$cnn=Conexion::getConexion();
		$mensaje="";
		try {
			$listaActivos= "SELECT df. * , re.nombre AS nomRevisor, re.apellido AS apRevisor
				FROM datosFormularios df
				INNER JOIN revisores re ON df.codigoRevisor = re.id
				WHERE df.codigo = '1'";
			$query=$cnn->prepare($listaActivos);
			$query->execute();
			return $query->fetchAll();
		} catch (Exception $e) {
			$mensaje=$ex->getMessage();
		}
		return $mensaje;
    }
    
    public function listarUsuariosInactivos(){
		$cnn=Conexion::getConexion();
		$mensaje="";
		try {
			$listaInactivos= "SELECT df. * , re.nombre AS nomRevisor, re.apellido AS apRevisor
				FROM datosFormularios df
				INNER JOIN revisores re ON df.codigoRevisor = re.id
				WHERE df.codigo = '2'";
			$query=$cnn->prepare($listaInactivos);
			$query->execute();
			return $query->fetchAll();
		} catch (Exception $e) {
			$mensaje=$ex->getMessage();
		}
		return $mensaje;
    }
    
    public function listarUsuariosEspera(){
		$cnn=Conexion::getConexion();
		$mensaje="";
		try {
			$listaEnEspera= "SELECT df. * , re.nombre AS nomRevisor, re.apellido AS apRevisor
				FROM datosFormularios df
				INNER JOIN revisores re ON df.codigoRevisor = re.id
				WHERE df.codigo = '3'";
			$query=$cnn->prepare($listaEnEspera);
			$query->execute();
			return $query->fetchAll();
		} catch (Exception $e) {
			$mensaje=$ex->getMessage();
		}
		return $mensaje;
	}
}
?>
